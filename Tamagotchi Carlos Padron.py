#Carlos Padron Taller Mascota Digital Tamagotchi
"""importando librerias necesarias"""
import random
import os
"""Generando el genero de la mascota"""
generolista = ["Masculino","Femenino"]
Genero = random.choice(generolista)
"""La clase para la mascota"""
class MiMascota:
    Felicidad = 10
    Hambre = 10
    Salud = 0
    sexo = Genero
    Nombre = ""

p = MiMascota()

def Alimentar():
    p.Hambre = p.Hambre + 2
    turno()
def Jugar():
    p.Felicidad = p.Felicidad + 3
    turno()
    print(random.choice(a.jugandolista))
    input("Precione Enter Para Continuar...")
def turno():
    p.Hambre = p.Hambre - 1
    p.Felicidad = p.Felicidad - 1

def status():
    if p.Felicidad == 0 or p.Hambre == 0 or p.Salud == 10:
        print(a.muerte1)
        print("La mascota se a muerto. ")
        input("Precione Enter Finalizar el Juego...")
        exit()
    elif p.Felicidad >= 8 and p.Hambre >= 9 and p.Hambre <= 10:
        print(a.happyfull)
    elif  p.Hambre > 10:
        print(random.choice(a.serioss))
        p.Salud = p.Salud - 1
    elif p.Salud == 1:
        print(a.final1)
    elif p.Felicidad < 5:
        print(random.choice(a.felicidadd))
    elif p.Hambre <= 4:
        print(a.hambre1)
    else:
        print(a.contento)




"""Los ASCII art para las mascotas. Tomadas de https://user.xmission.com/~emailbox/ascii_cats.html y modificadas para ser 
utilizadas en python"""
class arte:
    happyfull = """
                                      .                .                   
                                      :"""+chr(147)+"""-.          .-"""+chr(147)+""";                   
                                      |:`.`.__..__.'.';|                   
                                      || :-"""+chr(147)+"""      """+chr(147)+"""-; ||                   
                                      :;              :;                   
                                      /  .==.    .==.  """+chr(92)+"""                     
                                     :      _.--._      ;                  
                                     ; .--.' `--' `.--. :                  
                                    :   __;`      ':__   ;                 
                                    ;  '  '-._:;_.-'  '  :                 
                                    '.       `--'       .'                 
                                     ."""+chr(147)+"""-._          _.-"""+chr(147)+""".                  
                                   .'     """+chr(147)+""""""+chr(147)+"""------"""+chr(147)+""""""+chr(147)+"""     `.                
                                  /`-                    -'"""+chr(92)+"""               
                                 /`-                      -'"""+chr(92)+"""              
                                :`-   .'              `.   -';             
                                ;    /                  """+chr(92)+"""    :             
                               :    :                    ;    ;            
                               ;    ;                    :    :            
                               ':_:.'                    '.;_;'            
                                  :_                      _;               
                                  ; """+chr(147)+"""-._                -"""+chr(147)+""" :`-.     _.._   
                                  :_          ()          _;   """+chr(147)+"""--::__. `. 
                                   """+chr(92)+""""""+chr(147)+"""-                  -"""+chr(147)+"""/`._           : 
                                  .-"""+chr(147)+"""-.                 -"""+chr(147)+"""-.  """+chr(147)+""""""+chr(147)+"""--..____.' 
                                 /         .__  __.         """+chr(92)+"""              
                                : / ,       / """+chr(147)+""""""+chr(147)+""" """+chr(92)+"""       . """+chr(92)+""" ;          
                                 """+chr(147)+"""-:___..--"""+chr(147)+"""      """+chr(147)+"""--..___;-"""+chr(147)+""" """

    amargado = """
                                             .
                                            |'.._     __......._    _.-'.
                                            """+chr(92)+"""M"""+chr(92)+"""  ^'.-'          ''-/   /
                                             :M'._                ^'"""+chr(92)+""" ;
                                             'MMM'.                  '
                                            '.MMMM/ ;._            ,,|
                                              """+chr(92)+"""M; .   /.#"""+chr(92)+"""      /''  |
                                               ;     /d' #.___./d """+chr(92)+""" """+chr(147)+""":
                                               """+chr(147)+"""|    '---:dM   +"""+chr(92)+"""-/  ;
                                                :        _.d._'^.    """+chr(92)+"""
                                                ',      /    |   ;   ;
                                                  ..,___'.._,:"""+chr(92)+"""..' """+chr(92)+""""""+chr(92)+""" """
    gatoinicio = """
                               |        |
                               |"""+chr(92)+"""      /|
                               | """+chr(92)+"""____/ |
                               |  /"""+chr(92)+"""/"""+chr(92)+"""  |
                              .'___  ___`.
                             /  """+chr(92)+"""|/  """+chr(92)+"""|/  """+chr(92)+"""
            _.--------------( ____ __ _____)
         .-' """+chr(92)+"""  -. | | | | | """+chr(92)+""" ----"""+chr(92)+"""/---- /
       .'"""+chr(92)+"""  | | / """+chr(92)+"""` | | | |  `.  -'`-  .'
      /`  ` ` '/ / """+chr(92)+""" | | | | """+chr(92)+"""  `------'"""+chr(92)+"""
     /-  `-------.' `-----.       -----. `---.
    (  / | | | |  )/ | | | )/ | | | | | ) | | )
     `._________.'_____,,,/"""+chr(92)+"""_______,,,,/_,,,,/  """

    jugando1 = """
                                     ,
              ,-.       _,---._ __  / """+chr(92)+"""
             /  )    .-'       `./ /   """+chr(92)+"""
            (  (   ,'            `/    /|
             """+chr(92)+"""  `-"""+chr(147)+"""             """+chr(92)+"""'"""+chr(92)+"""   / |
              `.              ,  """+chr(92)+""" """+chr(92)+""" /  |
               /`.          ,'-`----Y   |
              (            ;        |   '
              |  ,-.    ,-'         |  /
              |  | (   |        CAP | /
              )  |  """+chr(92)+"""  `.___________|/
              `--'   `--' """
    jugando2 = """
                   .-o=o-.
               ,  /=o=o=o="""+chr(92)+""" .--.
              _|"""+chr(92)+"""|=o=O=o=O=|    """+chr(92)+"""
          __.'  a`"""+chr(92)+"""=o=o=o=(`"""+chr(92)+"""   /
          '.   a 4/`|.-"""+chr(147)+""""""+chr(147)+"""'`"""+chr(92)+""" """+chr(92)+""" ;'`)   .---.
            """+chr(92)+"""   .'  /   .--'  |_.'   / .-._)
             `)  _.'   /     /`-.__.' /
              `'-.____;     /'-.___.-'
                       `"""+chr(147)+""""""+chr(147)+""""""+chr(147)+"""` """

    jugando3 = """
                      __     __,
                      """+chr(92)+""",`~"""+chr(147)+"""~` /
      .-=-.           /    . ."""+chr(92)+"""
     / .-. """+chr(92)+"""          {  =    Y}=
    (_/   """+chr(92)+""" """+chr(92)+"""          """+chr(92)+"""      / 
           """+chr(92)+""" """+chr(92)+"""        _/`'`'`b
            """+chr(92)+""" `.__.-'`        """+chr(92)+"""-._
             |            '.__ `'-;_
             |            _.' `'-.__)
              """+chr(92)+"""    ;_..--'/     //  """+chr(92)+"""
              |   /  /   |     //    |
              """+chr(92)+"""  """+chr(92)+""" """+chr(92)+"""__)   """+chr(92)+"""   //    /
               """+chr(92)+"""__)        './/   .'
                             `'-'` """


    hambre1 = """
                                _.---.
                      |"""+chr(92)+"""---/|  / ) ca|
          ------------;     |-/ /|foo|---
                      )     (' / `---'
          ===========(       ,'==========
          ||   _     |      |
          || o/ )    |      | o
          || ( (    /       ;
          ||  """+chr(92)+""" `._/       /
          ||   `._        /|
          ||      |"""+chr(92)+"""    _/||
        __||_____.' )  |__||____________
         ________"""+chr(92)+"""  |  |_________________
                  """+chr(92)+""" """+chr(92)+"""  `-.
                   `-`---'   """
    muerte1 = """           ___
    ,_    '---'    _,
    """+chr(92)+""" `-._|"""+chr(92)+"""_/|_.-' /
     |   =)'T'(=   |
      """+chr(92)+"""   /`"""+chr(147)+"""`"""+chr(92)+"""   /
       '._"""+chr(92)+""") (/_.'
           | |
          /"""+chr(92)+""" /"""+chr(92)+"""
          """+chr(92)+""" T /
          (/ """+chr(92)+""")"""+chr(92)+"""
               ))
              ((
               """+chr(92)+""") """
    triste1 = """     _ _..._ __
    """+chr(92)+""")`    (` /
     /      `"""+chr(92)+"""
    |  d  b   |
    ="""+chr(92)+"""  Y    =/--..-="""+chr(147)+"""````"""+chr(147)+"""-.
      '.=__.-'               `"""+chr(92)+"""
         o/                 /"""+chr(92)+""" """+chr(92)+"""
          |                 | """+chr(92)+""" """+chr(92)+"""   / )
           """+chr(92)+"""    .--"""+chr(147)+""""""+chr(147)+"""`"""+chr(92)+"""    <   """+chr(92)+""" '-' /
          //   |      ||    """+chr(92)+"""   '---'
         ((,,_/      ((,,___/ """
    triste2 = """                           __ _..._ _ 
                           """+chr(92)+""" `)    `(/
                           /`       """+chr(92)+"""
                           |   d  b  |
             .-"""+chr(147)+"""````"""+chr(147)+"""=-..--"""+chr(92)+"""=    Y  /=
           /`               `-.__=.'
    _     / /"""+chr(92)+"""                 /o
   ( """+chr(92)+"""   / / |                 |
    """+chr(92)+""" '-' /   >    /`"""+chr(147)+""""""+chr(147)+"""--.    /
     '---'   /    ||      |   """+chr(92)+""""""+chr(92)+"""
             """+chr(92)+"""___,,))      """+chr(92)+"""_,,)) """
    contento = """       ,__         __,
        | """+chr(92)+""",-"""+chr(147)+""""""+chr(147)+""""""+chr(147)+"""-,/ |
        """+chr(92)+""".'_     _'./
        / <o>   <o>  """+chr(92)+"""
      >|      Y      |<
       """+chr(92)+"""   """+chr(92)+"""  :  /   /
        '.  '-^-'  .'
         /`-------'"""+chr(92)+"""
        /    >o<    """+chr(92)+"""
        |           |
      .-|           |-.
     |  |  |     |  |  |
     |  |  |     |  |  |
      """+chr(92)+""" |  |     |  | /
       )|  """+chr(92)+"""     /  |(`.
     .' /"""+chr(92)+"""__)._.(__/_,) )
     """+chr(147)+""""""+chr(147)+"""`     (_______,.' """
    serio = """                   /)
                  ((
                   ))
              ,   //,
             /,"""+chr(92)+"""="""+chr(147)+"""=/,"""+chr(92)+"""
            /` d   b `"""+chr(92)+"""
           ="""+chr(92)+""":.  Y  .:/=
            /'***o***'"""+chr(92)+"""
           ( (       ) )
           (,,)'-=-'(,,) """
    final1 = """           ,     ,
           |"""+chr(92)+"""."""+chr(147)+"""./|
          /       """+chr(92)+"""
         /  _   _  """+chr(92)+"""   ______
         """+chr(92)+"""==  Y  ==/"""+chr(147)+"""'`      `.
         /`-._^_.-'"""+chr(92)+"""     ,-  . """+chr(92)+"""
        /     `     """+chr(92)+"""   /     """+chr(92)+""" """+chr(92)+"""
        """+chr(92)+"""  """+chr(92)+""".___./  /_ _"""+chr(92)+"""     / /
         `, """+chr(92)+"""   / ,'  (,-----' /
           """+chr(147)+""""""+chr(147)+"""' '"""+chr(147)+""""""+chr(147)+"""     '------' """
    """listas para los random"""
    jugandolista = [jugando1, jugando2, jugando3]
    serioss = [final1, serio, amargado]
    felicidadd = [triste1,triste2,amargado]

a = arte()
def encabezado():
    """Encabezado creado usando http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20 y
    modificados para poder usarlos dentro de python"""
    print("""    $$\      $$\                                          $$\                     $$$$$$$\            
    $$$\    $$$ |                                         $$ |                    $$  __$$\           
    $$$$\  $$$$ | $$$$$$\   $$$$$$$\  $$$$$$$\  $$$$$$\ $$$$$$\    $$$$$$\        $$ |  $$ |$$\   $$\ 
    $$\$$\$$ $$ | \____$$\ $$  _____|$$  _____|$$  __$$""" + chr(92) + chr(92) + """_$$  _|   \____$$\       $$$$$$$  |$$ |  $$ |
    $$ \$$$  $$ | $$$$$$$ |\$$$$$$\  $$ /      $$ /  $$ | $$ |     $$$$$$$ |      $$  ____/ $$ |  $$ |
    $$ |\$  /$$ |$$  __$$ | \____$$\ $$ |      $$ |  $$ | $$ |$$\ $$  __$$ |      $$ |      $$ |  $$ |
    $$ | \_/ $$ |\$$$$$$$ |$$$$$$$  |\$$$$$$$\ \$$$$$$  | \$$$$  |\$$$$$$$ |      $$ |      \$$$$$$$ |
    \__|     \__| \_______|\_______/  \_______| \______/   \____/  \_______|      \__|       \____$$ |
                                                                                            $$\   $$ |
                                                                                            \$$$$$$  |
                                                                                             \______/ """)
def inicio():
    print("""Buenos Dias.
    Su mascota es un Gato de genero """ + p.sexo + """.""")
    Temp1 = (input("Cual es el Nombre: "))
    setattr(p, "Nombre", Temp1)
    print("El nombre de la Mascota es: " + p.Nombre)
    print(a.gatoinicio)
    input("Precione Enter Para Continuar...")
    os.system('cls' if os.name == 'nt' else 'clear')
def loopjuego():
    n = 100
    while n != 0:
        encabezado()
        status()
        n = input("""                                             Opciones
                                         1. Alimentar
                                         2. Jugar
                                         3. Nada
                                         0 . Salir
                                   Usando los Numeros haga su Eleccion. 
                                         Que decea Hacer: """)
        if n == "0":
            exit()
        elif n == "1":
            Alimentar()
        elif n == "2":
            Jugar()
        elif n == "3":
            turno()
            print("No Hiso nada este turno. ")
        else:
            input("                    Opcion Invalida, Precione enter para intertar nuevamente.")
            os.system('cls' if os.name == 'nt' else 'clear')

encabezado()
inicio()
loopjuego()
